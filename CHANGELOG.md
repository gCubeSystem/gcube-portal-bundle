
# Changelog for user gcube-portal-bundle

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v5.4.0]

- updated pom, maven parent and BOM
- authorization-client-legacy-jdk8 instead of authorization-client (fix patch problem)

## [v5.3.0] - 2023-12-04

- Removed the Cassandra Java client from common libs, now the portlets pass through the social service

## [v5.2.0] - 2023-03-31

- Removed some deps not needed anymore

## [v5.1.2] - 2022-11-04

- Updated social lib dependencies in Common CP

## [v5.1.1] - 2022-09-06

- Updated dependencies in Common CP

## [v5.1.0] - 2022-06-15

- Removed deprecated home library dependency

## [v5.0.2] - 2021-06-24

- Added support for authorization common AccessToken provider and common-gcube-calls in CP

## [v5.0.1] - 2021-03-25

- Added support for new Accounting libs

## [v5.0.0] - 2020-11-16

- Added support for keycloak as IAM

## [v4.6.0] - 2018-06-19

- Added Storage hub dependency

## [v4.3.0] - 2017-01-06

- Removed dismissed ASL Components, added portal auth library dependency

## [v3.6.0] - 2015-10-06

- Updated themes, added bootstrap support

## [v3.3.0] - 2014-10-27

- Updated themes for hashtag support, Updated dockbar for hashtag support

## [v3.0.0] - 2014-06-12

- gCube gCore Free Portal first release